package com.example.dawidwdowiak.kolokwium;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class zadanie1 extends AppCompatActivity {

    EditText input1, input2;
    TextView resultView;
    ListView listView;

    ArrayAdapter<String> listAdapter;

    String operations[] = new String[]{"Add", "Divide", "Factorial of first number"};
    final int ADD = 0;
    final int DIVIDE = 1;
    final int FACTORIAL = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadanie1);

        input1 = findViewById(R.id.zadanie1_input1);
        input2 = findViewById(R.id.zadanie1_input2);
        resultView = findViewById(R.id.zadanie1_resultView);

        listView = findViewById(R.id.zadanie1_listView);

        listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, operations);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String resultStr = "";

                switch(position)
                {
                    case ADD:
                        if(input1.getText().toString().length() != 0 &&
                                input2.getText().toString().length() != 0)
                        {
                            final double value1 = Double.valueOf(input1.getText().toString());
                            final double value2 = Double.valueOf(input2.getText().toString());
                            final double result = value1 + value2;

                            resultStr = "Result of addition is " + result;
                        }
                        else
                        {
                            resultStr = "We need two numbers!!";
                        }
                        break;

                    case DIVIDE:
                        if(input1.getText().toString().length() != 0 &&
                                input2.getText().toString().length() != 0)
                        {
                            final double value1 = Double.valueOf(input1.getText().toString());
                            final double value2 = Double.valueOf(input2.getText().toString());

                            if(value2 == 0)
                            {
                                resultView.setText("Cannot divide by 0");
                                return;
                            }

                            final double result = value1 / value2;

                            resultStr = "Result of division is " + result;
                        }
                        else
                        {
                            resultStr = "We need two numbers!!";
                        }
                        break;

                    case FACTORIAL:
                        if(input1.getText().toString().length() != 0)
                        {
                            final double value1 = Double.valueOf(input1.getText().toString());

                            if(value1 < 0)
                            {
                                resultView.setText("Cannot get factorial from negative number");
                                return;
                            }
                            if(value1 > 20)
                            {
                                resultView.setText("Cannot get factorial from value bigger than 20");
                                return;
                            }

                            final long factorialInput = Math.round(value1);
                            if(value1 > Math.floor(value1))
                            {
                                resultStr = "Rounding the number to: " + factorialInput + "\n";
                            }

                            final long factorialResult = getFactorial(factorialInput);
                            resultStr += "Factorial of " + factorialInput + " is " + factorialResult;
                        }

                        break;
                }

                resultView.setText(resultStr);
            }
        });
    }

    protected long getFactorial(long number)
    {
        if(number <= 2) return number;
        return number * getFactorial(number - 1);
    }
}
