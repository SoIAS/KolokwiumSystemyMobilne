package com.example.dawidwdowiak.kolokwium;

/**
 * Created by Dawid Wdowiak on 1/20/2018.
 */

public class GameItem
{
    // nie mialem czasu na zrobienie enkapsulacji
    public int itemId;
    public int itemType;
    public boolean hidden;
    public boolean available;
    public String country;

    GameItem(int itemType, String country)
    {
        this.itemType = itemType;
        this.hidden = false;
        this.available = false;
        this.country = country;
    }
}
