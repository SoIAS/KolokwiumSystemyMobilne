package com.example.dawidwdowiak.kolokwium;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

// TODO: READ ME
// Kod nie jest zbytio dobry w tym zadaniu, zabraklo mi czasu wiec zaczalem robic bardzo szybko
// Gra dzialac dziala, ale nie jest to dobry kod


public class zadanie3 extends AppCompatActivity
{

    GridView gridView;
    Button playButton;

    final int maxDifferentTypes = 6;
    final int amountPerType = 2;
    final int numOfGameItems = maxDifferentTypes * amountPerType;

    ArrayList<GameItem> gameItems;
    GameAdapter adapter;

    boolean playing;

    ArrayList<GameItem> selectedItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadanie3);


        playButton = findViewById(R.id.zadanie3_beginGameButton);
        gridView = findViewById(R.id.zadanie3_gridView);

        gameItems = new ArrayList<>();
        selectedItems = new ArrayList<>();

        setupNewGame();

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beginGame();
            }
        });

    }

    protected void setupNewGame()
    {
        gameItems.clear();

        String countries[] = getResources().getStringArray(R.array.countries);

        ArrayList<Integer> types = getGameElements();
        Random rand = new Random();
        for(int i = 0; i < numOfGameItems; ++i)
        {
            int randomTypeIndex = rand.nextInt(types.size());
            int randomType = types.get(randomTypeIndex);
            types.remove(randomTypeIndex);

            gameItems.add(new GameItem(randomType, countries[randomType]));
        }

        playing = false;

        updateView();
    }

    protected ArrayList<Integer> getGameElements()
    {
        // todo for more than 6

        ArrayList<Integer> l = new ArrayList<>();
        for(int i = 0; i < maxDifferentTypes; ++i)
        {
            l.add(i);
            l.add(i);
        }

        return l;
    }

    protected void updateView()
    {
        if(adapter == null)
        {
            ArrayList<GameItem> c = (ArrayList<GameItem>)gameItems.clone();
            adapter = new GameAdapter(c, this);
            gridView.setAdapter(adapter);
        }
        else
        {
            ArrayList<GameItem> c = (ArrayList<GameItem>)gameItems.clone();
            adapter.clear();
            adapter.addAll(c);
            adapter.notifyDataSetChanged();
        }
    }

    protected void beginGame()
    {
        for(int i = 0; i < numOfGameItems; ++i)
        {
            gameItems.get(i).hidden = true;
            gameItems.get(i).available = true;
        }
        playing = true;
        updateView();
    }

    public void gameItemClicked(View view)
    {
        if(playing != true) return;

        final int itemIndex = (int)view.getTag();

        if(gameItems.get(itemIndex).hidden == false ||
                gameItems.get(itemIndex).available == false)
        {
            return;
        }

        gameItems.get(itemIndex).hidden = false;
        selectedItems.add(gameItems.get(itemIndex));

        if(selectedItems.size() == amountPerType)
        {
            final int itemType = selectedItems.get(0).itemType;

            boolean same = true;
            for(int i = 1; i < amountPerType; ++i)
            {
                if(selectedItems.get(i).itemType != itemType)
                {
                    same = false;
                }
            }

            if(same)
            {
                for(GameItem item : selectedItems)
                {
                    item.available = false;
                }
            }
            else
            {
                for (GameItem item : selectedItems)
                {
                    item.hidden = true;
                }
            }

            selectedItems.clear();
            if(isWon())
            {
                Toast.makeText(this, "You have won", Toast.LENGTH_SHORT).show();
                playing = false;
                setupNewGame(); // todo
            }
        }

        updateView();
    }

    public boolean isWon()
    {
        if(playing == false) return false;

        for(int i = 0; i < numOfGameItems; ++i)
        {
            if(gameItems.get(i).available == true)
            {
                return false;
            }
        }

        return true;
    }

}
