package com.example.dawidwdowiak.kolokwium;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO: ZMIEN KLASE INTENTU ABY URUCHOMIC ODPOWIEDNIE ZADANIE
        Intent intent = new Intent(this, zadanie1.class);
        startActivity(intent);
    }
}
