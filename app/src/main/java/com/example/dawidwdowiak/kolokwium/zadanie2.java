package com.example.dawidwdowiak.kolokwium;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class zadanie2 extends AppCompatActivity {

    TextView resultView;
    EditText inputText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadanie2);

        resultView = findViewById(R.id.zadanie2_result);
        inputText = findViewById(R.id.zadanie2_input);

        ((Button)findViewById(R.id.zadanie2_checkButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(inputText.getText().toString().length() == 0)
                {
                    return;
                }

                final int inputValue = Integer.parseInt(inputText.getText().toString());

                String str;
                if(isPrimary(inputValue))
                {
                    str = inputValue + " is a primary number";
                }
                else
                {
                    str = inputValue + " is not a primary number";

                    final int closestPrimary = findClosestPrimary(inputValue);
                    if(closestPrimary == 0)
                    {
                        str += "\nThere is no closest lower primary number";
                    }
                    else
                    {
                        str += "\nCloses lower primary number is " + closestPrimary;
                    }
                }

                resultView.setText(str);


            }
        });
    }

    protected boolean isPrimary(int number)
    {
        if(number <= 1) return false;
        if(number <= 3) return true;

        if(number % 2 == 0 || number % 3 == 0) return false;

        for(int i = 5; i * i <= number; i+=6)
        {
            if(number % i == 0 || number % (i+2) == 0) return false;
        }

        return true;
    }

    protected int findClosestPrimary(int number)
    {
        for(int i = number - 1; i > 1; --i)
        {
            if(isPrimary(i))
            {
                return i;
            }
        }

        return 0;
    }
}
