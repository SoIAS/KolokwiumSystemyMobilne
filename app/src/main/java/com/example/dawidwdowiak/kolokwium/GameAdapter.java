package com.example.dawidwdowiak.kolokwium;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Dawid Wdowiak on 1/20/2018.
 */

public class GameAdapter extends ArrayAdapter
{
    private ArrayList<GameItem> data;
    Context context;

    private static class ViewHolder
    {
        Button button;
    }

    public GameAdapter(ArrayList<GameItem> data, Context context)
    {
        super(context, R.layout.game_item_layout, data);
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    @Override
    public GameItem getItem(int position)
    {
        return data.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        View result;


        if(convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_item_layout, parent, false);

            viewHolder.button = convertView.findViewById(R.id.gameItem_Button);

            convertView.setTag(viewHolder);
            result = convertView;
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        GameItem gameItem = getItem(position);
        viewHolder.button.setText(gameItem.hidden ? "Hidden" : gameItem.country);

        viewHolder.button.setTag(position);
        return result;
    }

}
